# nextjs template

### Notes

- add command `"prebuild": "pnpm exec generate edgeql-js"`, if edgedb query builder is used

### Useful Commands

- `pnpm prebuild` : Generates EdgeDB query builder (auto detects remote/local instance)
- `./scripts/../migrate.sh` : Generate migration/apply it to EdgeDB instance
- `./scripts/../dev-migrate.sh` : Generate dev migration/apply it to EdgeDB instance
- `./scripts/../watch.sh` : Start schema development session
- `./scripts/testdb/generate-querybuilder.sh` : Generate query builder for "testdb" database
- `./scripts/testdb/seed.sh` : Seed EdgeDB test database ("testdb" must be created)
- `./scripts/testdb/seed-data.edgeql` : EdgeQL seed queries

### Reference Documentation

- [Next](https://nextjs.org/docs)
- [Tailwind](https://tailwindcss.com/docs/installation)
- [EdgeDB](https://www.edgedb.com/docs/clients/js/index)
- [Zod](https://zod.dev)  # added but not implemented
- [TS-Rest](https://ts-rest.com/docs/intro)  # added but not implemented
- [HeroIcons](https://github.com/tailwindlabs/heroicons)
