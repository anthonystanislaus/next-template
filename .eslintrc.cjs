/** @type {import("eslint").Linter.Config} */
const config = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: true,
  },
  plugins: ["@typescript-eslint"],
  extends: [
    "plugin:@typescript-eslint/recommended-type-checked",
    "plugin:@typescript-eslint/stylistic-type-checked",
    "prettier",
    "next/core-web-vitals",
  ],
  rules: {
    // These rules are enabled in stylistic-type-checked above.
    "@typescript-eslint/no-unsafe-member-access": "off", // for clsx inline and edgedb query builder
    "@typescript-eslint/no-unsafe-return": "off", // for edgedb query builder
    "@typescript-eslint/no-unsafe-assignment": "off", // for clsx inline
    "@typescript-eslint/no-unsafe-call": "off", // for clsx inline
    "@typescript-eslint/array-type": "off",
    "@typescript-eslint/consistent-type-definitions": "off",
    "@typescript-eslint/consistent-type-imports": [
      "warn",
      {
        prefer: "type-imports",
        fixStyle: "inline-type-imports",
      },
    ],
    "@typescript-eslint/no-unused-vars": ["warn", { argsIgnorePattern: "^_" }],
    "@typescript-eslint/require-await": "off",
    "@typescript-eslint/no-misused-promises": [
      "error",
      {
        checksVoidReturn: { attributes: false },
      },
    ],
  },
};

module.exports = config;
