#!/usr/bin/env bash

set -e

edgedb migration create --squash
edgedb migrate --dev-mode
