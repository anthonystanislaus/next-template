# INSERT USERS
with
  raw_data := <json>to_json($$[
  {
    "name": "User 1",
  },
  {
    "name": "User 2",
  }
  ]$$),
for u in json_array_unpack(raw_data) union (
  insert User {
    name := <str>u["name"],
  }
);


# INSERT EXERCISES
with
  raw_data := <json>to_json($$[
  {
    name: "Squats",
    description: "A compound exercise that targets the legs and glutes",
    exercise_type: ExerciseType.PUSH,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.LEGS,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Push-ups",
    description: "A compound exercise that targets the chest, triceps, and shoulders",
    exercise_type: ExerciseType.PUSH,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.CHEST,
    muscle_group: MuscleGroup.UPPER,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Planks",
    description: "A core exercise that targets the abs, back, and shoulders",
    exercise_type: ExerciseType.FLEXIBILITY,
    repetition_scheme: RepetitionScheme.DURATION,
    target_muscle: TargetMuscle.CORE,
    muscle_group: MuscleGroup.CORE,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Jumping Jacks",
    description: "An aerobic exercise that targets the whole body",
    exercise_type: ExerciseType.CARDIO,
    repetition_scheme: RepetitionScheme.DURATION,
    target_muscle: TargetMuscle.CALVES,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Jump Squats",
    description: "A compound exercise that targets the legs and glutes",
    exercise_type: ExerciseType.PUSH,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.LEGS,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Rows",
    description: "A compound exercise that targets the back, biceps, and shoulders",
    exercise_type: ExerciseType.PULL,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.BACK,
    muscle_group: MuscleGroup.UPPER,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Crunches",
    description: "A core exercise that targets the abs",
    exercise_type: ExerciseType.FLEXIBILITY,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.ABS,
    muscle_group: MuscleGroup.CORE,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Jump Rope",
    description: "An aerobic exercise that targets the whole body",
    exercise_type: ExerciseType.CARDIO,
    repetition_scheme: RepetitionScheme.DURATION,
    target_muscle: TargetMuscle.LOWER,
    muscle_group: MuscleGroup.CALVES,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Lunges",
    description: "A compound exercise that targets the legs and glutes",
    exercise_type: ExerciseType.PUSH,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.QUADS,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Shoulder Press",
    description: "A compound exercise that targets the shoulders and triceps",
    exercise_type: ExerciseType.PUSH,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.DELTS,
    muscle_group: MuscleGroup.UPPER,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Bicycle Crunches",
    description: "A core exercise that targets the abs",
    exercise_type: ExerciseType.FLEXIBILITY,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.ABS,
    muscle_group: MuscleGroup.CORE,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Jumping Rope",
    description: "An aerobic exercise that targets the whole body",
    exercise_type: ExerciseType.CARDIO,
    repetition_scheme: RepetitionScheme.DURATION,
    target_muscle: TargetMuscle.CALVES,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Step-ups",
    description: "A compound exercise that targets the legs and glutes",
    exercise_type: ExerciseType.PUSH,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.LEGS,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Pull-ups",
    description: "A compound exercise that targets the back, biceps, and shoulders",
    exercise_type: ExerciseType.PULL,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.BACK,
    muscle_group: MuscleGroup.UPPER,
    difficulty: Difficulty.ADVANCED
  },
  {
    name: "Russian Twists",
    description: "A core exercise that targets the abs",
    exercise_type: ExerciseType.FLEXIBILITY,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.ABS,
    muscle_group: MuscleGroup.CORE,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Walking",
    description: "An aerobic exercise that targets the whole body",
    exercise_type: ExerciseType.CARDIO,
    repetition_scheme: RepetitionScheme.DURATION,
    target_muscle: TargetMuscle.CALVES,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Jumping Lunges",
    description: "A compound exercise that targets the legs and glutes",
    exercise_type: ExerciseType.PUSH,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.CALVES,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.INTERMEDIATE
  },
  {
    name: "Leg Raises",
    description: "A core exercise that targets the abs and lower back",
    exercise_type: ExerciseType.FLEXIBILITY,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.ABS,
    muscle_group: MuscleGroup.CORE,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Running",
    description: "An aerobic exercise that targets the whole body",
    exercise_type: ExerciseType.CARDIO,
    repetition_scheme: RepetitionScheme.ENDURANCE,
    target_muscle: TargetMuscle.CALVES,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.BEGINNER
  },
  {
    name: "Jumping Squats",
    description: "A compound exercise that targets the legs and glutes",
    exercise_type: ExerciseType.PUSH,
    repetition_scheme: RepetitionScheme.REPS,
    target_muscle: TargetMuscle.LEGS,
    muscle_group: MuscleGroup.LOWER,
    difficulty: Difficulty.INTERMEDIATE
  },
]$$),
for e in json_array_unpack(raw_data) union (
  insert Exercise {
    name := <str>e["name"],
    description := <str>e["description"]
    exercise_type := <str>e["exercise_type"],
    repetition_scheme := <str>e["repetition_scheme"],
    target_muscle := <str>e["target_muscle"],
    muscle_group := <str>e["muscle_group"],
    difficulty := <str>e["difficulty"],
  }
);
