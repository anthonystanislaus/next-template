#!/usr/bin/env bash

# seed must be outside the project root due to edgedb query generator

set -e

db='edgedb'
input_file="./scripts/$db/seed-data.edgeql"

edgedb query --file "$input_file"
