import { createEnv } from "@t3-oss/env-nextjs";
import { z } from "zod";

export const env = createEnv({
  /**
   * Specify your server-side environment variables schema here. This way you can ensure the app
   * isn't built with invalid env vars.
   */
  server: {
    NODE_ENV: z.enum(["development", "test", "production"]),
    // EDGEDB_DSN: z.string().url(),
    // EDGEDB_INSTANCE: z.string(),
    EDGEDB_SECRET_KEY: z.string(),
    EDGEDB_DATABASE: z.string(),
    EDGEDB_CLIENT_TLS_SECURITY: z.enum([
      "insecure",
      "no_host_verification",
      "strict",
      "default",
    ]),
  },

  /**
   * Specify your client-side environment variables schema here. This way you can ensure the app
   * isn't built with invalid env vars. To expose them to the client, prefix them with
   * `NEXT_PUBLIC_`.
   */
  client: {
    // NEXT_PUBLIC_CLIENTVAR: z.string(),
  },

  /**
   * You can't destruct `process.env` as a regular object in the Next.js edge runtimes (e.g.
   * middlewares) or client-side, so we need to destruct manually.
   */
  runtimeEnv: {
    NODE_ENV: process.env.NODE_ENV,
    // EDGEDB_DSN: process.env.EDGEDB_DSN,
    // EDGEDB_INSTANCE: process.env.EDGEDB_INSTANCE,
    EDGEDB_SECRET_KEY: process.env.EDGEDB_SECRET_KEY,
    EDGEDB_DATABASE: process.env.EDGEDB_DATABASE,
    EDGEDB_CLIENT_TLS_SECURITY: process.env.EDGEDB_CLIENT_TLS_SECURITY,
    // NEXT_PUBLIC_CLIENTVAR: process.env.NEXT_PUBLIC_CLIENTVAR,
  },
  /**
   * Run `build` or `dev` with `SKIP_ENV_VALIDATION` to skip env validation. This is especially
   * useful for Docker builds.
   */
  skipValidation: !!process.env.SKIP_ENV_VALIDATION,
  /**
   * Makes it so that empty strings are treated as undefined. `SOME_VAR: z.string()` and
   * `SOME_VAR=''` will throw an error.
   */
  emptyStringAsUndefined: true,
});
