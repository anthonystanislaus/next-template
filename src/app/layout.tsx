import "@ui/global.css";
import { jetbrains } from "@src/ui/fonts";

export const metadata = {
  title: {
    template: "%s | next-edgedb-template",
    default: "next-edgedb-template",
  },
  description: "Next with EdgeDB",
  icons: [{ rel: "icon", url: "/favicon.ico" }],
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`${jetbrains.className} antialiased`}>{children}</body>
    </html>
  );
}
