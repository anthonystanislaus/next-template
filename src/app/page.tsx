import { ArrowFatLinesRight } from "@phosphor-icons/react";

export default function Page() {
  return (
    <main className="flex min-h-screen flex-col justify-start p-6">
      <ArrowFatLinesRight size={32} />
    </main>
  );
}
