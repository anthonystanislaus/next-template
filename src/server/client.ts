import { env } from "@src/env";
import { createClient } from "edgedb";

export const DATABASE = createClient({
  // dsn: env.EDGEDB_DSN,
  database: env.EDGEDB_DATABASE,
  tlsSecurity: env.EDGEDB_CLIENT_TLS_SECURITY,
});

/*
CLIENT OPTIONS
  dsn?: string;
  instanceName?: string;
  credentials?: string;
  credentialsFile?: string;
  host?: string;
  port?: number;
  database?: string;
  user?: string;
  password?: string;
  secretKey?: string;
  serverSettings?: any;
  tlsCA?: string;
  tlsCAFile?: string;
  tlsSecurity?: TlsSecurity;
  timeout?: number;
  waitUntilAvailable?: Duration | number;
  logging?: boolean;
*/
